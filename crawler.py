import os

# Function to crawl the directory, yield the content of each file, the file name, and extract content between '^' pairs
def crawl_dir(directory):
    for root, dirs, files in os.walk(directory):
        for filename in files:
            file_path = os.path.join(root, filename)
            with open(file_path, 'r') as file:
                content = file.read()  # Read the whole file
                template = content  # Original content
                tags = []
                tag_start = 0
                while True:
                    tag_start = content.find('^', tag_start)
                    if tag_start == -1:
                        break
                    tag_start += 1  # Move past '^'
                    tag_end = content.find('^', tag_start)
                    if tag_end == -1:
                        break
                    tag = content[tag_start:tag_end].strip()
                    tags.append(tag)
                    tag_start = tag_end + 1  # Move past the second '^'
                file_content = content
                yield filename, file_content, tags  # Yield the filename and tags
