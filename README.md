# SSSB (Simple Static Site Builder)



## Getting started

Simply write HTML as you always would, but with added 'tags'

the `^file_name^` tag must be placed first, it will make the html file inherit from the file named `file_name.html`

the `^<-fill^` tag is used in templates and indicates where inheritor content will be injected.

any other tag of `^<-file_name^` will simply insert the content of file name into the tag's location.
