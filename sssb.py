import os
import crawler

# Get the current working directory
current_dir = os.getcwd()

# Get an array of page tuples
pages = crawler.crawl_dir(current_dir+"/pages")

named_pages = {}
for page in pages:
    filename, file_content, tags = page
    named_pages[filename] = (file_content, tags)

def fetch_page(page_name):
    file_content, tags = named_pages[page_name]
    if len(tags) != 0 and not tags[0].startswith('<-'):
        print("uses template")
        # Remove the template definition
        file_content = file_content.replace('^'+tags[0]+'^', "")
        # Insert the file into the template
        template_name = tags[0]
        template_content = fetch_page(template_name + ".html")
        file_content = template_content.replace('^<-fill^', file_content)
    for tag in tags:
        if tag.startswith('<-'):
            if tag[2:]+".html" in named_pages:
                file_content = file_content.replace('^'+tag+'^', fetch_page(tag[2:]+".html"))
            else:
                print("ERR: non existing "+tag[2:]+" attempted to add to "+page_name)
    return file_content

for page in named_pages:
    print("\n||| page: "+page+" |||\n")
    with open("out/"+page, 'w') as file:
        file.write(fetch_page(page))
